import {Trainee} from './trainee';

export interface AppStore {
  items: Trainee[];
  selectedItem: Trainee;
  searchTerm: string;
}
