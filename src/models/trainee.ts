export class Trainee {
    id: Number;
    name: String;
    date: Date;
    grade: String;
    subject: String;
    email: String;
    address: String;
    city: String;
    country: String;
    zip: String;
}