import {NgModule} from '@angular/core';

import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    { path: "", redirectTo: "/data", pathMatch: 'full'},
    { path: "data", loadChildren: "./../components/data/data.module#DataModule" },
    { path: "analysis", loadChildren: "./../components/analysis/analysis.module#AnalysisModule"},
    { path: "monitor", loadChildren: "./../components/monitor/monitor.module#MonitorModule"}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule{}