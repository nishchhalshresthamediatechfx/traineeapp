import {Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import {Trainee} from '../../models/trainee';
import {
    TraineeActions, 
    LoadTraineesSuccessAction, 
    SaveTraineeAction, 
    SaveTraineeSuccessAction, 
    AddTraineeAction, 
    AddTraineeSuccessAction,
    DeleteTraineeAction,
    DeleteTraineeSuccessAction,
    LoadTraineesAction,
    ResetBlankTraineeAction
} from '../actions/trainees.action';
import * as _ from 'lodash';

export type TraineesState = Trainee[];

const initialState: TraineesState = [];

export function traineesReducer(state = initialState, action: LoadTraineesSuccessAction
    | SaveTraineeAction
    | SaveTraineeSuccessAction
    | AddTraineeAction
    | AddTraineeSuccessAction
    | DeleteTraineeAction
    | DeleteTraineeSuccessAction
    | LoadTraineesAction
    | ResetBlankTraineeAction
): TraineesState {
    switch (action.type) {
        case TraineeActions.LOAD_TRAINEES_SUCCESS: {
            return action.payload;
        }
        case TraineeActions.ADD_TRAINEE_SUCCESS: {
            return [...state, action.payload];
        }
        case TraineeActions.SAVE_TRAINEE_SUCCESS: {
            let index = _.findIndex(state, {id: action.payload.id});
            if (index >= 0) {
                return [
                    ...state.slice(0, index),
                    action.payload,
                    ...state.slice(index + 1)
                ];
            }
            return state;
        }
        case TraineeActions.DELETE_TRAINEE_SUCCESS: {
            return state.filter(hero => {
                return hero.id !== action.payload.id;
            });
        }
        default: {
            return state;
        }
    }
}