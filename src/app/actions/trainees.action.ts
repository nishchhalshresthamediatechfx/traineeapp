import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';

import {Trainee} from '../../models/trainee';

@Injectable()
export class TraineeActions {
    static LOAD_TRAINEES = '[Trainee] Load Trainees';
    loadTrainees(): LoadTraineesAction {
        return {
            type: TraineeActions.LOAD_TRAINEES
        };
    }

    static LOAD_TRAINEES_SUCCESS = '[Trainee] Load Trainees Success';
    loadTraineesSuccess(trainees): LoadTraineesSuccessAction {
        return {
            type: TraineeActions.LOAD_TRAINEES_SUCCESS,
            payload: trainees
        };
    }

    static RESET_BLANK_TRAINEE = '[Trainee] Reset Blank Trainee';
    resetBlankTrainee(): ResetBlankTraineeAction {
        return {
            type: TraineeActions.RESET_BLANK_TRAINEE
        };
    }

    static SAVE_TRAINEE = '[Trainee] Save Trainee';
    saveTrainee(trainee): SaveTraineeAction {
        return {
            type: TraineeActions.SAVE_TRAINEE,
            payload: trainee
        };
    }

    static SAVE_TRAINEE_SUCCESS = '[Trainee] Save Trainee Success';
    saveTraineeSuccess(trainee): SaveTraineeSuccessAction {
        return {
            type: TraineeActions.SAVE_TRAINEE_SUCCESS,
            payload: trainee
        };
    }

    static ADD_TRAINEE = '[Trainee] Add Trainee';
    addTrainee(trainee): AddTraineeAction {
        return {
            type: TraineeActions.ADD_TRAINEE,
            payload: trainee
        };
    }

    static ADD_TRAINEE_SUCCESS = '[Trainee] Add Trainee Success';
    addTraineeSuccess(trainee): AddTraineeSuccessAction {
        return {
            type: TraineeActions.ADD_TRAINEE_SUCCESS,
            payload: trainee
        };
    }

    static DELETE_TRAINEE = '[Trainee] Delete Trainee';
    deleteTrainee(trainee): DeleteTraineeAction {
        return {
            type: TraineeActions.DELETE_TRAINEE,
            payload: trainee
        };
    }

    static DELETE_TRAINEE_SUCCESS = '[Trainee] Delete Trainee Success';
    deleteTraineeSuccess(trainee): DeleteTraineeSuccessAction {
        return {
            type: TraineeActions.DELETE_TRAINEE_SUCCESS,
            payload: trainee
        };
    }
}

@Injectable()
export class LoadTraineesAction implements Action {
    readonly type: string = TraineeActions.LOAD_TRAINEES;
    payload? : any
    constructor() { }
}

@Injectable()
export class LoadTraineesSuccessAction implements Action {
    readonly type: string = TraineeActions.LOAD_TRAINEES_SUCCESS;
    constructor(public payload: any[]) { }
}

@Injectable()
export class ResetBlankTraineeAction implements Action {
    readonly type: string = TraineeActions.RESET_BLANK_TRAINEE;
    payload? : any;
    constructor() { }
}

@Injectable()
export class SaveTraineeAction implements Action {
    readonly type: string = TraineeActions.SAVE_TRAINEE;
    constructor(public payload: any[]) { }
}

@Injectable()
export class SaveTraineeSuccessAction implements Action {
    readonly type: string = TraineeActions.SAVE_TRAINEE_SUCCESS;
    constructor(public payload: any[]) { }
}

@Injectable()
export class AddTraineeAction implements Action {
    readonly type: string = TraineeActions.ADD_TRAINEE;
    constructor(public payload: any[]) { }
}

@Injectable()
export class AddTraineeSuccessAction implements Action {
    readonly type: string = TraineeActions.ADD_TRAINEE_SUCCESS;
    constructor(public payload: any[]) { }
}

@Injectable()
export class DeleteTraineeAction implements Action {
    readonly type: string = TraineeActions.DELETE_TRAINEE;
    constructor(public payload: any[]) { }
}

@Injectable()
export class DeleteTraineeSuccessAction implements Action {
    readonly type: string = TraineeActions.DELETE_TRAINEE_SUCCESS;
    constructor(public payload: any[]) { }
}