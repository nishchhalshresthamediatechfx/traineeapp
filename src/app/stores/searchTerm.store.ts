export function searchTerm(state: any = null, {type, payload}) {
    switch (type) {
      case 'SEARCH':
        return payload;
      default:
        return state;
    }
  };
  