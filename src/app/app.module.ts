import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { DataModule } from '../components/data/data.module';
import { AnalysisModule } from '../components/analysis/analysis.module';
import { MonitorModule } from '../components/monitor/monitor.module';
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DataModule,
    AnalysisModule,
    MonitorModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
