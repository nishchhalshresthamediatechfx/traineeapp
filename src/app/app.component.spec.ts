import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should have three li',async()=>{
    const fixture = TestBed.createComponent(AppComponent);
    let nativeElement = fixture.nativeElement;    
    let li = nativeElement.querySelectorAll('ul li');
    expect(li.length).toEqual(3);
  });

 it(`first menu should be Data`,async()=>{
    const fixture=TestBed.createComponent(AppComponent);
    let nativeElement=fixture.nativeElement;    
    let lis = nativeElement.querySelector('ul li:nth-child(1) a');
    expect(lis.innerHTML).toEqual('Data');
 });
 it(`second menu should be Analysis`,async()=>{
  const fixture=TestBed.createComponent(AppComponent);
  let nativeElement=fixture.nativeElement;    
  let lis = nativeElement.querySelector('ul li:nth-child(2) a');
  expect(lis.innerHTML).toEqual('Analysis');
});
it(`third menu should be monitor`,async()=>{
  const fixture=TestBed.createComponent(AppComponent);
  let nativeElement=fixture.nativeElement;    
  let lis = nativeElement.querySelector('ul li:nth-child(3) a');
  expect(lis.innerHTML).toEqual('Monitor');
});

});
