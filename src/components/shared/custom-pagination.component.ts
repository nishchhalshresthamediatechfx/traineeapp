import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { PaginationInstance } from 'ng2-pagination';

@Component({
    selector: 'custom-pagination',
    templateUrl: 'custom-pagination.component.html'
})
export class CustomPaginationComponent implements OnInit {
    @Input() isPageDetails: boolean;
    @Output() pageChange: EventEmitter<any> = new EventEmitter();
    @Output() itemsPerPageChange: EventEmitter<any> = new EventEmitter();
    public config: PaginationInstance = {
        id: 'custom',
        itemsPerPage: 5,
        currentPage: 1,
        totalItems: 0
    };

    constructor() { }

    ngOnInit() { }

    changeItemsPerPage(itemsPerPage: number) {
        this.config.itemsPerPage = itemsPerPage;
        this.itemsPerPageChange.emit(itemsPerPage);
    }

    handlePageChange($event) {
        this.config.currentPage = $event;
        this.pageChange.emit($event);
    }
}
