import { async, ComponentFixture, TestBed, inject, tick, fakeAsync } from '@angular/core/testing';
import { StoreModule, Store } from '@ngrx/store';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { DataListComponent } from './data-list.component';
import { CustomPaginationComponent} from '../../components/shared/custom-pagination.component';
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';
import { Ng2PaginationModule } from 'ng2-pagination';
import { TraineeService } from '../../providers/trainee.service';
import { TraineeMockService } from '../../providers/trainee-mock.service';
import {AppStore} from '../../models/app-store';
import {items } from './../../app/stores/items.store';
import {searchTerm } from './../../app/stores/searchTerm.store';
import {selectedItem } from './../../app/stores/selectedItem.store';
import { Trainee } from '../../models/trainee';

describe('DataListComponent', () => {
  let component: DataListComponent;
  let fixture: ComponentFixture<DataListComponent>;
  let traineeService: TraineeService = null;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        Ng2PaginationModule,
        Ng2FilterPipeModule,
        StoreModule.forRoot({items, searchTerm, selectedItem})
      ],
      declarations: [ DataListComponent, CustomPaginationComponent ],
      providers: [{ provide: TraineeService, useClass: TraineeMockService }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  beforeEach(inject([TraineeService], (_traineeService: TraineeService) => {
    traineeService = _traineeService;
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have two items in list', fakeAsync(() => {   
    const fixture=TestBed.createComponent(DataListComponent); 
    traineeService.loadItems();
    fixture.detectChanges();
    tick();                  
    fixture.detectChanges(); 
    let nativeElement=fixture.nativeElement;  
    let lis = nativeElement.querySelector('div table tbody');
    expect(lis.children.length).toEqual(2);
 }));

  

});
