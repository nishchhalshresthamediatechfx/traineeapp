import { Component, OnInit, Input, ViewChild } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import { Trainee } from '../../models/trainee';
import { TraineeService } from '../../providers/trainee.service';
import {PaginationInstance} from 'ng2-pagination';
import { CustomPaginationComponent} from '../../components/shared/custom-pagination.component';
import {TraineeComponent} from '../../components/trainee/trainee.component';
import {AppStore} from '../../models/app-store';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.css']
})
export class DataListComponent implements OnInit {  
  trainees: Observable<Array<Trainee>>;
  searchTerm: Observable<string>;
  userFilter: any = { id: '' };
  @Input()
  config: PaginationInstance;  
  constructor(
    private traineeService: TraineeService,
    private store: Store<AppStore>
  ) { 
    this.trainees = traineeService.items;

    this.searchTerm = store.select(state => state.searchTerm);    
    this.searchTerm.subscribe( v => this.userFilter.id = v );
    traineeService.loadItems();
  }


  selectItem(model: Trainee) {
    this.store.dispatch({type: 'SELECT_ITEM', payload: model});
    // this.isAdd = false;
    // this.isEdit = true;
  }

  ngOnInit() {
    // this.bottomPagination.config = this.config;
    if(this.config == null) {
      this.config = {
        id: 'custom',
        itemsPerPage: 5,
        currentPage: 1,
        totalItems: 0
      }
    }
  }

}
