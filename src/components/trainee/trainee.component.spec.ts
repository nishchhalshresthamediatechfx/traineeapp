import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';

import { StoreModule, Store } from '@ngrx/store';
import { TraineeComponent } from './../trainee/trainee.component';
import { TraineeService } from '../../providers/trainee.service';
import { TraineeMockService } from '../../providers/trainee-mock.service';
import {AppStore} from '../../models/app-store';
import {items } from './../../app/stores/items.store';
import {searchTerm } from './../../app/stores/searchTerm.store';
import {selectedItem } from './../../app/stores/selectedItem.store';

describe('TraineeComponent', () => {
  let component: TraineeComponent;
  let fixture: ComponentFixture<TraineeComponent>;
  let traineeService: TraineeService = null;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule,
        StoreModule.forRoot({items, searchTerm, selectedItem})],
      declarations: [ TraineeComponent ],
      providers: [{ provide: TraineeService, useClass: TraineeMockService}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraineeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(inject([TraineeService], (_traineeService: TraineeService) => {
    traineeService = _traineeService;
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form should be invalid when empty', () => {
    expect(component.traineeForm.valid).toBeFalsy();
  });

  it('id field should be required', () => {
    let errors = {};
    let id = component.traineeForm.controls['id'];
    errors = id.errors || {};
    expect(errors['required']).toBeTruthy(); 
  });

  it('submitting a form is allowed if form is valid', () => {
    expect(component.traineeForm.valid).toBeFalsy();
    component.traineeForm.controls['id'].setValue(3);
    component.traineeForm.controls['name'].setValue("Nishchhal Shrestha");
    component.traineeForm.controls['email'].setValue("nishchhal@gmail.com");
    expect(component.traineeForm.valid).toBeTruthy();
  });

  it('id field should be unique', () => {
    let errors = {};
    traineeService.loadItems();
    let id = component.traineeForm.controls['id'];
    id.setValue(1);
    errors = id.errors || {};
    expect(errors['notUnique']).toBeTruthy(); 
  });  

  it('email field should be valid email', () => {
    let errors = {};
    let email = component.traineeForm.controls['email'];
    email.setValue("nishchhal@gmail");
    errors = email.errors || {};
    expect(errors['isEmail']).toBeTruthy(); 

    email.setValue("nishchhal.shrestha@mediatechfx.com");
    errors = email.errors || {};
    expect(errors['isEmail']).toBeUndefined(); 
  });
});
