import { Component, OnInit, Input, Output, EventEmitter, Directive, Attribute, OnChanges, forwardRef } from '@angular/core';
import { EmailValidator,NG_VALIDATORS,Validator, Validators,AbstractControl,ValidatorFn, FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { Trainee } from '../../models/trainee';

import { TraineeService } from '../../providers/trainee.service';
@Component({
  selector: 'trainee',
  templateUrl: './trainee.component.html',
  styleUrls: ['./trainee.component.css']
})
export class TraineeComponent implements OnInit {
  trainee: Trainee;
  @Input() set traineeItem (value: Trainee){
    this.trainee = Object.assign({}, value);
  }
  @Input() isAdd: boolean;
  @Input() isEdit: boolean;
  @Input() countries: boolean;
  @Input() cities: boolean;
  @Output() onSubmitted = new EventEmitter(); 

  traineeForm: FormGroup;

  emailRegex = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
  constructor(
    private traineeService: TraineeService,
    private fb: FormBuilder
  ) {
    this.createForm();
   }

  ngOnInit() {
    this.onChanges();
  }

  ngOnChanges() {
    this.traineeForm.reset({
      id: this.trainee.id,
      name: this.trainee.name,
      date: this.trainee.date,
      city: this.trainee.city,
      country: this.trainee.country,
      address: this.trainee.address,
      grade: this.trainee.grade,
      subject: this.trainee.subject,
      zip: this.trainee.zip,
      email: this.trainee.email
    });

    if(!this.isAdd) {
      this.traineeForm.get('id').disable();
    } else {
      this.traineeForm.get('id').enable();
    }

    if(!this.isAdd && !this.isEdit) {
      this.traineeForm.get('name').disable();
      this.traineeForm.get('email').disable();
      this.traineeForm.get('city').disable();
      this.traineeForm.get('country').disable();
      this.traineeForm.get('grade').disable();
      this.traineeForm.get('subject').disable();
      this.traineeForm.get('date').disable();
      this.traineeForm.get('zip').disable();
      this.traineeForm.get('address').disable();
    } else {
      this.traineeForm.get('name').enable();
      this.traineeForm.get('email').enable();
      this.traineeForm.get('city').enable();
      this.traineeForm.get('country').enable();
      this.traineeForm.get('grade').enable();
      this.traineeForm.get('subject').enable();
      this.traineeForm.get('date').enable();
      this.traineeForm.get('zip').enable();
      this.traineeForm.get('address').enable();
    }
  }

  onChanges(): void {
    this.traineeForm.valueChanges.subscribe(val => {
      if(this.isEdit) {
        let traineeTemp = this.prepareSaveTrainee();

        this.trainee.name = traineeTemp.name;
        if(traineeTemp.email && this.emailRegex.test(traineeTemp.email.toString())) {
          this.trainee.email = traineeTemp.email;
        }
        this.trainee.address = traineeTemp.address;
        this.trainee.city = traineeTemp.city;
        this.trainee.country = traineeTemp.country;
        this.trainee.date = traineeTemp.date;
        this.trainee.grade = traineeTemp.grade;
        this.trainee.subject = traineeTemp.subject;
        this.trainee.zip = traineeTemp.zip;
        this.traineeService.updateItem(this.trainee);
      }
    });
  }

  createForm() {
    this.traineeForm = this.fb.group({
      id: [null, Validators.compose([Validators.required, this.checkIfIdIsUnique.bind(this)])],
      name: [''],      
      email: ['', this.isEmail.bind(this)],
      city: '',
      country: '',
      date: null,
      grade: '',
      subject: '',
      zip: '',
      address: ''
    });
  }

  onSubmit() {
    if (this.traineeForm.valid) {
      let traineeTemp = this.prepareSaveTrainee();
      if(traineeTemp.id.toString().trim() == "") {
        return;
      }
      if(!this.traineeService.isIdUnique(traineeTemp.id)) {
        return;
      }
      if(this.trainee.email && !this.emailRegex.test(traineeTemp.email.toString())) {
        return;
      }    
      this.trainee = traineeTemp;
      this.traineeService.createItem(this.trainee);
      this.ngOnChanges();    
      this.onSubmitted.emit();
    } else {            
      this.validateAllFormFields(this.traineeForm);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  prepareSaveTrainee(): Trainee {
    const formModel = this.traineeForm.value;
    const saveTrainee: Trainee = {
      id: formModel.id,
      name: formModel.name,
      date: formModel.date,
      grade: formModel.grade,
      subject: formModel.subject,
      email: formModel.email,
      address:formModel.address,
      city: formModel.city,
      country: formModel.country,
      zip: formModel.zip
    };
    return saveTrainee;
  }

  revert() { this.ngOnChanges(); }

  isFieldValid(field: string) {
    return !this.traineeForm.get(field).valid && this.traineeForm.get(field).touched;
  }

  checkIfIdIsUnique(fieldControl: FormControl){
    if(fieldControl.value == null || fieldControl.value.toString().trim() == "") {
      return null;
    }
    return this.traineeService.isIdUnique(fieldControl.value) ? null : { notUnique: true };
  }

  isEmail(fieldControl: FormControl) {
    if(fieldControl.value == null || fieldControl.value.trim() == "") {
      return null;
    }
    return this.emailRegex.test(fieldControl.value) ? null : { isEmail: true };
  }
}
