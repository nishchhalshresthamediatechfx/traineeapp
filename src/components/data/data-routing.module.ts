import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';

import { DataComponent }    from './data.component';

@NgModule({
  imports: [RouterModule.forChild([
    { path: '', component: DataComponent, data: { shouldDetach: true} }
  ])],
  exports: [RouterModule]
})
export class DataRoutingModule {}