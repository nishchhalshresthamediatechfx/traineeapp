import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';

import { DataComponent } from './data.component';
import { DataListComponent } from '../data-list/data-list.component';
import { FilterComponent } from '../filter/filter.component';
import { TraineeComponent } from './../trainee/trainee.component';
import { TraineeService } from '../../providers/trainee.service';

import {RouteReuseStrategy} from '@angular/router';
import {CustomReuseStrategy} from './../../providers/reuse-strategy';
import {items } from './../../app/stores/items.store';
import {selectedItem} from './../../app/stores/selectedItem.store';
import {searchTerm} from './../../app/stores/searchTerm.store';
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';


import { Ng2PaginationModule } from 'ng2-pagination';
import { CustomPaginationComponent} from '../../components/shared/custom-pagination.component';

import { DataRoutingModule } from './data-routing.module';


@NgModule({
  imports:      [ CommonModule, ReactiveFormsModule, FormsModule, DataRoutingModule, Ng2PaginationModule, Ng2FilterPipeModule
  , StoreModule.forRoot({items, selectedItem, searchTerm}) ],
  declarations: [ DataComponent, TraineeComponent, CustomPaginationComponent, FilterComponent, DataListComponent ],
  providers:    [ TraineeService, {provide: RouteReuseStrategy, useClass: CustomReuseStrategy}]
})
export class DataModule { }