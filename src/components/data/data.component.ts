import { Component, OnInit, Input, ViewChild } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import { CustomPaginationComponent} from '../../components/shared/custom-pagination.component';
import { Trainee } from '../../models/trainee';
import { TraineeService } from '../../providers/trainee.service';
import {TraineeComponent} from '../../components/trainee/trainee.component';
import {AppStore} from '../../models/app-store';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import { FilterComponent } from '../filter/filter.component';
import { PaginationInstance } from 'ng2-pagination';
@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {  
  @ViewChild('bottomPagination') private bottomPagination: CustomPaginationComponent;
  @ViewChild('traineeForm') private traineeForm: TraineeComponent;  
  countries: Array<string> = ["Nepal", "India", "United States", "Israel", "Australia"];
  cities: Array<string> = ["Kathmandu", "Delhi", "New York", "Jerusalem", "Sydney"];
  isAdd: boolean = false;
  isEdit: boolean = false;
  trainee: Observable<Trainee>;
  userFilter: any = { id: '' };
  config: PaginationInstance = {
      id: 'custom',
      itemsPerPage: 5,
      currentPage: 1,
      totalItems: 0
    };
  constructor(
    private traineeService: TraineeService,
    private store: Store<AppStore>
  ) {        
     this.trainee = store.select(state => state.selectedItem);
     this.trainee.subscribe(item => {
      if(item && item.id) {
         this.isAdd = false;
         this.isEdit = true;
      }
     })
   }

  ngOnInit() {
    this.bottomPagination.config = this.config;
  }
  ngAfterViewInit(){
    
  }
  private _save(){   
    this.traineeForm.onSubmit();
  }
  private _reset(){
    this.isAdd = true;
    this.isEdit = false;
    this.store.dispatch({type: 'SELECT_ITEM', payload: new Trainee()}); 
  }
  add(){
    if(!this.isAdd){
      this.isEdit = false;
      this.isAdd = true;
      this.store.dispatch({type: 'SELECT_ITEM', payload: new Trainee()});   
    }
    else if(this.isAdd){
      this._save();
    }
  }
  selectItem(model: Trainee) {
    this.store.dispatch({type: 'SELECT_ITEM', payload: model});
    this.isAdd = false;
    this.isEdit = true;
  }
  remove(){
    let traineeItemTemp : Trainee;
    this.trainee.map((traineeItem: Trainee) => { return traineeItem; }).subscribe((traineeItem) => { traineeItemTemp = traineeItem;  });
    this.traineeService.deleteItem(traineeItemTemp);    
    this._reset();
  }
  pageChanged(){
  }
  itemsPerPageChange(){
  }
  traineeAdded() {
    this._reset();
  }
}
