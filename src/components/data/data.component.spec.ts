import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StoreModule, Store } from '@ngrx/store';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, Component} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';

import { Ng2FilterPipeModule } from 'ng2-filter-pipe';
import { Ng2PaginationModule } from 'ng2-pagination';

import { DataComponent } from './data.component';
import { TraineeService } from '../../providers/trainee.service';
import { TraineeMockService } from '../../providers/trainee-mock.service';
import {AppStore} from '../../models/app-store';
import {items } from './../../app/stores/items.store';
import {searchTerm } from './../../app/stores/searchTerm.store';
import {selectedItem } from './../../app/stores/selectedItem.store';
import {FilterComponent} from './../filter/filter.component';
import { DataListComponent } from './../data-list/data-list.component';
import {TraineeComponent} from './../trainee/trainee.component';
import { CustomPaginationComponent} from '../../components/shared/custom-pagination.component';

describe('DataComponent', () => {
  let component: DataComponent;
  let fixture: ComponentFixture<DataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({items, searchTerm, selectedItem}),
        Ng2FilterPipeModule,
        Ng2PaginationModule,
        ReactiveFormsModule
      ],
      declarations: [ 
        DataComponent, 
        FilterComponent,
        DataListComponent,
        TraineeComponent,
        CustomPaginationComponent        
      ],
      providers: [{ provide: TraineeService, useClass: TraineeMockService} ]     
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataComponent);    
    component = fixture.componentInstance;    
    fixture.detectChanges();
  });

  it('should create data', () => {
    expect(component).toBeTruthy();
  }); 
});
