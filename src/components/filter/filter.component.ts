import { Component, OnInit,Output ,EventEmitter} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {AppStore} from '../../models/app-store';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

@Output() searchKey = new EventEmitter()
  constructor(
    private store: Store<AppStore>
  ) { 
      
  }

  onChange(event) {
    this.store.dispatch({ type: 'SEARCH', payload: event.target.value});
  }

  ngOnInit() {
    
   }
}
