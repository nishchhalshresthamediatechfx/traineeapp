import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';

import { AnalysisComponent }    from './analysis.component';

@NgModule({
  imports: [RouterModule.forChild([
    {
        path: '', component: AnalysisComponent,
    }
  ])],
  exports: [RouterModule]
})
export class AnalysisRoutingModule {}