import { Injectable } from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import { Trainee } from '../models/trainee';
import { AppStore } from '../models/app-store';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
@Injectable()
export class TraineeService {
    items: Observable<Array<Trainee>>;
    filteredItems: Observable<Array<Trainee>>;
    /**
     *
     */
    keys = {
        trainees: 'trainees'
    };
    constructor(private store: Store<AppStore>) {
        this.items = store.select(state => state.items);
    }
    getTrainees(): Observable<Trainee[]> {
        // let trainees: Array<Trainee> = JSON.parse(localStorage.getItem(this.keys.trainees));
        // if (!trainees) {
        //     trainees = new Array<Trainee>();
        // }
        let trainees = new Array<Trainee>();
        return Observable.of(trainees);
    }
    loadItems() {
        this.getTrainees()
          .map(payload => ({ type: 'ADD_ITEMS', payload }))
          .subscribe(action => this.store.dispatch(action));
      }
    createItem(item: Trainee) {        
        Observable.of(item)
          .map(payload => ({ type: 'CREATE_ITEM', payload }))
          .subscribe(action => this.store.dispatch(action));
      }
    
      updateItem(item: Trainee) {
        Observable.of(item)
          .subscribe(action => this.store.dispatch({ type: 'UPDATE_ITEM', payload: item }));
      }    
    deleteItem(item: Trainee) {
        Observable.of(item)
            .subscribe(action => this.store.dispatch({ type: 'DELETE_ITEM', payload: item }));
    }
    
    isIdUnique(id: Number): boolean {
        let trainees: Array<Trainee>;
        this.items.subscribe((item) => { return trainees = item; });
    
        if(trainees.filter(function(item) { return item.id == id; }).length > 0) {
            return false;
        }
        return true;
    }
}