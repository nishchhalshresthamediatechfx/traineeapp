import { Injectable } from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import { Trainee } from '../models/trainee';
import { AppStore } from '../models/app-store';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
@Injectable()
export class TraineeMockService {
    items: Observable<Array<Trainee>>;
    /**
     *
     */
    keys = {
        trainees: 'trainees'
    };
    constructor(private store: Store<AppStore>) {
        this.items = store.select(state => state.items);
    }
    getTrainees(): Observable<Trainee[]> {
        // let trainees: Array<Trainee> = JSON.parse(localStorage.getItem(this.keys.trainees));
        // if (!trainees) {
        //     trainees = new Array<Trainee>();
        // }
        let trainees = new Array<Trainee>();
        trainees.push(
            {id:1,address:'Ktm',city:'Ktm',country:'Nepal',date:new Date(),email:'amit@gmail.com',grade:'5',name:'Amit',subject:'test',zip:'977'},
            {id:2,address:'Ktm',city:'Ktm',country:'Nepal',date:new Date(),email:'nishchhal@gmail.com',grade:'5',name:'Nishchhal',subject:'test',zip:'977'}
        );
        // this.items=Observable.of(trainees);
        return Observable.of(trainees);
    }
    loadItems() {
        this.getTrainees()
          .map(payload => ({ type: 'ADD_ITEMS', payload }))
          .subscribe(action => this.store.dispatch(action));
      }
    createItem(item: Trainee) {        
        // Observable.of(item)
        //   .map(payload => ({ type: 'CREATE_ITEM', payload }))
        //   .subscribe(action => this.store.dispatch(action));
      }
    
      updateItem(item: Trainee) {
        // Observable.of(item)
        //   .subscribe(action => this.store.dispatch({ type: 'UPDATE_ITEM', payload: item }));
      }    
    deleteItem(item: Trainee) {
        // Observable.of(item)
        //     .subscribe(action => this.store.dispatch({ type: 'DELETE_ITEM', payload: item }));
    }
    
    isIdUnique(id: Number): boolean {
        let trainees: Array<Trainee>;
        this.items.subscribe((item) => { return trainees = item; });
    
        if(trainees.filter(function(item) { return item.id == id; }).length > 0) {
            return false;
        }
        return true;
    }
}